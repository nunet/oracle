# Check more details about the job validation process on the
# following link: https://gitlab.com/nunet/oracle/-/issues/5


import logging
from src.jobs_logging import log_bin


logger = logging.getLogger(__name__)


def validate_job(job_status, log_url_path):
    # First check exit status
    container_exit_error = check_container_exit_error(job_status)
    logger.info("Container exit status has been checked, checking the logs")

    # If no error on job status, checking logs 
    stderr_logs, no_logs = log_bin.get_stderr_logs(log_url_path)
    if container_exit_error and no_logs:
        logger.info("Refund > We detected an exit error and did not find logs")
        return "refund"

    parsed_stderr = log_bin.parse_logs(stderr_logs)
    last_log_fine = log_bin.is_last_stderr_log_empty(parsed_stderr)

    if ( not container_exit_error and last_log_fine):
        logger.info("Withdraw > No error was found on the job logs")
        return "withdraw"
    elif (( container_exit_error and last_log_fine ) or (not container_exit_error and not last_log_fine )):
        # If last stderr is not empty, we consider as an exit error
        logger.info("Distribute > We detected an exit error")
        return "distribute"

    logger.info("Distribute > We detected an exit error")

    return "distribute"

def check_container_exit_error(job_status):
    # checking exit status of container
    if job_status == "finished without errors":
        return False
    elif job_status == "finished with errors":
        return True
    else:
        logger.warning("Invalid job status received. Check if the container is still running")
        raise ValueError("Invalid job status")
