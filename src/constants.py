import os
from dotenv import load_dotenv
from src.tests.mock_stub import funding_request


load_dotenv()

HASKELL_SERVER_URL = os.environ["HASKELL_SERVER_URL"]

# variables for funding request
SP_ADDR = funding_request.service_provider_addr
CP_ADDR = funding_request.compute_provider_addr
ESTIMATED_PRICE = funding_request.estimated_price
