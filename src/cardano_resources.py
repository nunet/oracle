import requests
from src.constants import HASKELL_SERVER_URL


def process_fund_req(sp_addr, cp_addr, estimated_price, is_test_env):
    if is_test_env:
        return (
            "metadata-hash",
            "withdraw-hash",
            "refund-hash",
            "distribute-50-hash",
            "distribute-75-hash",
        )

    url = f"http://{HASKELL_SERVER_URL}:3877/request_service"
    params = {"user": sp_addr, "computeProvider": cp_addr, "ntxAmount": estimated_price}
    res = requests.post(url=url, json=params)
    data = res.json()
    meta_hash = data["metaDataHash"]
    withdraw_hash = data["withdrawHash"]
    refund_hash = data["refundHash"]
    distribute_50_hash = data["distribute50Hash"]
    distribute_75_hash = data["distribute75Hash"]
    return (
        meta_hash,
        withdraw_hash,
        refund_hash,
        distribute_50_hash,
        distribute_75_hash,
    )


def process_reward_req(meta_hash, action_hash, is_test_env):
    if is_test_env:
        return (
            "signature-datum",
            "message-hash-datum",
            "datum",
            "signature-action",
            "message-hash-action",
            "action",
        )

    url = f"http://{HASKELL_SERVER_URL}:3877/request_reward"
    params = {"metaDataHash": meta_hash, "actionHash": action_hash}
    res = requests.post(url=url, json=params)
    data = res.json()
    sign_datum = data["sigData"]
    msg_hash_datum = data["sigDataHash"]
    datum = data["sigDataDatum"]
    sign_action = data["sigAction"]
    msg_hash_action = data["sigActionHash"]
    action = data["sigActionDatum"]
    return sign_datum, msg_hash_datum, datum, sign_action, msg_hash_action, action
