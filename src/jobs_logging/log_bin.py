import requests


empty_log_msg = "no updates from docker container"


def is_last_stderr_log_empty(parsed_stderr):
    last_log = parsed_stderr[-1][0]
    if empty_log_msg in last_log:
        return True

    return False


def parse_logs(logs):
    # TODO: this might change as we change NuNet LogsBin
    final_ordered_list = []
    current_log_piece = ""
    current_timestamp = 0  # Starting with 0 for the first content
    empty_logs = 0

    for line in logs.split("\n"):
        if "NUNETLOGAPPEND" not in line:
            current_log_piece += line.strip() + "\n"
        else:
            #  Append the current log piece with the current timestamp
            if empty_log_msg in current_log_piece:
                empty_logs += 1
            final_ordered_list.append((current_log_piece.strip(), current_timestamp))
            #  Update the timestamp after the last '=' for the next content
            current_timestamp = int(line.split("=")[-1])
            current_log_piece = ""

    # Handle last log
    if empty_log_msg in current_log_piece:
        empty_logs += 1
    final_ordered_list.append((current_log_piece.strip(), current_timestamp))

    return final_ordered_list


def get_logs(log_type, log_url_path):
    if log_type not in ["stdout", "stderr"]:
        raise ValueError("log_type must be 'stdout' or 'stderr'")

    if len(log_url_path) < 0:
        return "", True

    url = f"{log_url_path}/{log_type}"
    response = requests.get(url)

    if response.status_code == 200:
        return response.text, False
    elif response.status_code == 404:
        return "", True
    else:
        raise Exception(f"Unexpected status code: {response.status_code}")


def get_stderr_logs(log_url_path):
    return get_logs("stderr", log_url_path)


def get_stdout_logs(log_url_path):
    return get_logs("stdout", log_url_path)
