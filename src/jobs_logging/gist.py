from src.utils.clients import get_github_client


def check_if_last_is_exit_error(gist):
    # Check if last stderr was updated in the last revision
    # which would mean that the program throw and an error and exited

    # TODO: check if the first revision in the list is always the last updated
    last_revision = gist.history[0]
    last_stderr = last_revision.files["stderr.log"]

    # Check if stderr raw_url already existed in any previous revision
    last_stderr_url = last_stderr.raw_url
    for idx in range(len(gist.history)):
        if idx == 0:
            continue
        if gist.history[idx].files["stderr.log"].raw_url == last_stderr_url:
            return False

    return True


def check_for_error(gist):
    stderr = gist.files["stderr.log"].content

    whole_content = stderr.split("\n")
    content = whole_content[0].strip().lower()
    if "no updates" in content and "stderr" in content:
        return False

    return True


def get_gist(log_path):
    gist_id = parse_log_path(log_path)
    g = get_github_client()
    gist = g.get_gist(gist_id)
    return gist


def parse_log_path(log_path):
    gist_id = log_path.split('/')[-1]
    return gist_id
