import pytest
from unittest.mock import patch, MagicMock
from src import cardano_resources


@pytest.fixture
def expected_fund_response():
    return (
        "metadata-hash",
        "withdraw-hash",
        "refund-hash",
        "distribute-50-hash",
        "distribute-75-hash",
    )

@pytest.fixture
def expected_reward_response():
    return (
        "signature-datum",
        "message-hash-datum",
        "datum",
        "signature-action",
        "message-hash-action",
        "action",
    )

@patch("requests.post")
def test_process_fund_req(mock_post, expected_fund_response):
    mock_response = MagicMock()
    mock_response.json.return_value = {
        "metaDataHash": "metadata-hash",
        "withdrawHash": "withdraw-hash",
        "refundHash": "refund-hash",
        "distribute50Hash": "distribute-50-hash",
        "distribute75Hash": "distribute-75-hash",
    }
    mock_post.return_value = mock_response

    result = cardano_resources.process_fund_req("sp_addr", "cp_addr", 100, is_test_env=True)

    assert result == expected_fund_response

@patch("requests.post")
def test_process_reward_req(mock_post, expected_reward_response):
    mock_response = MagicMock()
    mock_response.json.return_value = {
        "sigData": "signature-datum",
        "sigDataHash": "message-hash-datum",
        "sigDataDatum": "datum",
        "sigAction": "signature-action",
        "sigActionHash": "message-hash-action",
        "sigActionDatum": "action",
    }
    mock_post.return_value = mock_response

    result = cardano_resources.process_reward_req("meta_hash", "action_hash", is_test_env=True)

    assert result == expected_reward_response
