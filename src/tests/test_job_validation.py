import pytest
from src import job_validation


"""
Testing with real API:
The following test depends on LogsBin API availability
(TODO: inject logs directly into validate_job() when calling it)
"""


@pytest.mark.parametrize("job_status, log_path, expected_result", [
    ("finished without errors",
     "https://log.nunet.io/api/v1/logbin/13d26b81-4324-402b-9f7f-cbd05afafb67/raw",
     "withdraw"),

    ("finished without errors",
     "https://log.nunet.io/api/v1/logbin/01472e48-c430-46f1-8b8a-40c6d1e5cfd1/raw",
     "distribute"),
])
def test_validate_job(job_status, log_path, expected_result):
    """Test the job validation process"""
    assert job_validation.validate_job(job_status, log_path) == expected_result

"""
Testing with real API:
The following tests depend on the GIST API, GIST token and availability of the GISTs
"""

# TODO: add one more param Gist sucesfull job log
# TODO: these GIST logs should be in the NuNet github account
# the tests are based on gist logs from several people, so this may
# result in errors in case the gist is deleted by those
# @pytest.mark.parametrize("job_status, log_path, expected_result", [
#     ("finished without errors", "42e86f264c89be54e3351e2373c92edf", "withdraw"),
#     ("finished without errors", "7889f3fb3f0cb1d4bc9c8497dd22846a", "refund"),
#     ("finished without errors", "db9080d70f1f62b87c6fc7589767a314", "refund"),
#     ("finished with errors", "42e86f264c89be54e3351e2373c92edf", "refund"),
# ])
# def test_validate_job(job_status, log_path, expected_result):
#     """Test the job processing"""
#     assert job_validation.validate_job(job_status, log_path) == expected_result
