import pytest
import grpc
from concurrent import futures
from contextlib import contextmanager

from src import oracle
from main import OracleServicer
from src.tests import mock_stub
from protos.compiled import oracle_pb2, oracle_pb2_grpc


# TODO: these tests basically test the gRPC calls but it is not
# on its ideal form, we have to improve it. We'll keep commented
# because we couldn't make to run the Oracle on test_mode to use
# the TEST private Cardano key.

# Note: It's not a big problem because, again, these tests on this file
# just test the gRPC calls. The core logic is being tested on the other
# files.

# @pytest.fixture
# def funding_req_params():
#     return oracle_pb2.FundingRequest()
#
#
# @pytest.fixture
# def withdraw_req_params():
#     return oracle_pb2.WithdrawRequest(
#         job_status="finished without errors",
#         log_path="42e86f264c89be54e3351e2373c92edf"
#         )
#
#
# @contextmanager
# def gen_server_and_stub():
#     # TODO: Investigate a better way of testing this gRPC server
#     # (we are manually starting the server here, but we should use the
#     # run_grpc_server (main.py) function used in production. The problem of using it
#     # is when closing the server)
#     port = "50052"
#     server = grpc.server(futures.ThreadPoolExecutor(max_workers=3))
#     oracle_pb2_grpc.add_OracleServicer_to_server(OracleServicer(), server)
#     server.add_insecure_port(f'localhost:{port}')
#     server.start()
#     try:
#         with grpc.insecure_channel(f'localhost:{port}') as channel:
#             yield oracle_pb2_grpc.OracleStub(channel)
#     finally:
#         server.stop(0)
# 
# 
# def test_gRPC_server_fund_req(funding_req_params):
#     with gen_server_and_stub() as stub:
#         signature, msg = mock_stub.request_funding(stub, funding_req_params)
# 
#         errors = []
#         if not signature:
#             errors.append("Signature error")
#         if not msg:
#             errors.append("Message error")
# 
#         assert not errors, "errors occured:\n{}".format("\n".join(errors))
# 
# 
# def test_gRPC_server_withdraw_req(withdraw_req_params):
#     with gen_server_and_stub() as stub:
#         signature, msg, reward_type = mock_stub.request_withdraw(
#                 stub, withdraw_req_params)
# 
#         errors = []
#         if not signature:
#             errors.append("Signature error")
#         if not msg:
#             errors.append("Message error")
#         if not reward_type:
#             errors.append("Reward Type error")
# 
#         assert not errors, "errors occured:\n{}".format("\n".join(errors))
