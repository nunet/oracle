import pytest

from src.jobs_logging import log_bin


@pytest.fixture
def mock_logs_with_empty_last_log():
    logs = """
    First logging
    Testing ONE

    NUNETLOGAPPEND==============10

    Second logging
    Testing TWO

    NUNETLOGAPPEND==============230

    NUNETLOGAPPEND==============1000

    NUNETLOGAPPEND==============30000

    """

    return logs

@pytest.fixture
def mock_logs_with_non_empty_last_log():
    logs = """
    First logging
    Testing ONE

    NUNETLOGAPPEND==============10

    Second logging
    Testing TWO

    NUNETLOGAPPEND==============230

    NUNETLOGAPPEND==============1000

    NUNETLOGAPPEND==============30000

    Last Logging
    Final Test

    """

    return logs


def test_parse_logs_last_empty(mock_logs_with_empty_last_log):
    expected = [
            ('First logging\nTesting ONE', 0),
            ('Second logging\nTesting TWO', 10),
            ('', 230), ('', 1000), ('', 30000)
            ]
    parsed_logs = log_bin.parse_logs(mock_logs_with_empty_last_log)
    assert parsed_logs == expected
