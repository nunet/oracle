import sys, os
import logging
import grpc
sys.path.append(os.getcwd())
from protos.compiled import oracle_pb2, oracle_pb2_grpc


if len(sys.argv) == 2 and sys.argv[1] == "test":
    address = "localhost"
    port = "50052"
    is_test_env = True
else:
    address = "dev.nunet.io"
    port = "40052"


funding_request = oracle_pb2.FundingRequest(
    service_provider_addr="addr_test1qqrjsetnua2ed5xavru9kv9xz9pmatg5ahr5uvfnzpm77l50a2cgqr0q0d90snp8p4paln7n3v58q8qs9687wha9dkzqts2zzg",
    compute_provider_addr="addr_test1qpw6v74gc0qxhphp5e4szkwa823yvp295tmrn7trrhygtv7zvnlg4uq3wvwdc42rlg6yyr8tm8cv06efmsljk99s05pqnm3s3u",
    estimated_price=1
)

reward_request_list = {
    "reward_option_1" : oracle_pb2.RewardRequest(
        job_status="finished without errors",
        job_duration=5,
        estimated_job_duration=10,
        log_path="https://log.nunet.io/api/v1/logbin/72d04064-040c-4cf9-bc46-8a1f9d09bc21/raw",
        metadata_hash="616464725f74657374317171726a7365746e7561326564357861767275396b7639787a39706d61746735616872357576666e7a706d37376c3530613263677172307130643930736e7038703470616c6e376e33763538713871733936383777686139646b7a717473327a7a67616464725f7465737431717a38326b3567616d636a38346836356c616c65686e3978687a3732657370723430736e65717932776e376e796b6367766b367936367076393263336d6176327970353964726b3265766e77636d333977346775793834796c357673746a6d34347230",
        withdraw_hash="616e776b6e62736c6565",
        refund_hash="6978706c627174696567",
        distribute_50_hash="796c71676b72726f6463",
        distribute_75_hash="636e796178706579787a"
    ),
    "reward_option_2" : oracle_pb2.RewardRequest(
        job_status="finished with errors",
        job_duration=5,
        estimated_job_duration=10,
        log_path="https://log.nunet.io/api/v1/logbin/3d9de6d4-44c9-4484-91fc-7388130d71fc/raw",
        metadata_hash="616464725f74657374317171726a7365746e7561326564357861767275396b7639787a39706d61746735616872357576666e7a706d37376c3530613263677172307130643930736e7038703470616c6e376e33763538713871733936383777686139646b7a717473327a7a67616464725f7465737431717a38326b3567616d636a38346836356c616c65686e3978687a3732657370723430736e65717932776e376e796b6367766b367936367076393263336d6176327970353964726b3265766e77636d333977346775793834796c357673746a6d34347230",
        withdraw_hash="6b6b717a6a77787a6468",
        refund_hash="71666b68746763677a64",
        distribute_50_hash="666b6172686e6966646f",
        distribute_75_hash="6272677a6d626a616276"
    )
}


def request_funding(stub, params):
    res = stub.ValidateFundingReq(params)
    print(res)


def request_reward(stub, params):
    res = stub.ValidateRewardReq(params)
    print(res)


def run():
    with grpc.insecure_channel(f'{address}:{port}') as channel:
        print(f'INFO: Calling gRPC server on {address}:{port}')
        stub = oracle_pb2_grpc.OracleStub(channel)
        print("-------------- Request funding validation --------------")
        request_funding(stub, funding_request)
        print("-------------- Request withdraw validation --------------")
        for reward_request in reward_request_list.values():
            request_reward(stub, reward_request)



if __name__ == "__main__":
    logging.basicConfig()
    run()
