import logging

from src import cardano_resources
from src.job_validation import validate_job


logger = logging.getLogger(__name__)


def validate_fund_req(sp_addr, cp_addr, estimated_price, is_test_env):
    (
        meta_hash,
        withdraw_hash,
        refund_hash,
        distribute_50_hash,
        distribute_75_hash,
    ) = cardano_resources.process_fund_req(
        sp_addr, cp_addr, estimated_price, is_test_env
    )
    return (
        meta_hash,
        withdraw_hash,
        refund_hash,
        distribute_50_hash,
        distribute_75_hash,
    )


def validate_reward_req(request, is_test_env):
    logger.info("Validating job completeness for withdraw request")
    validate_reward_type = validate_job(request.job_status, request.log_path)
    reward_type, action_hash = get_action_hash(request, validate_reward_type)
    (
        sign_datum,
        msg_hash_datum,
        datum,
        sign_action,
        msg_hash_action,
        action,
    ) = cardano_resources.process_reward_req(
        request.metadata_hash, action_hash, is_test_env
    )
    return (
        reward_type,
        sign_datum,
        msg_hash_datum,
        datum,
        sign_action,
        msg_hash_action,
        action,
    )


def get_action_hash(request, validate_reward_type):
    logger.info("Getting action_hash for wallet server")
    if validate_reward_type == "withdraw":
        return "withdraw", request.withdraw_hash
    elif validate_reward_type == "refund":
        return "refund", request.refund_hash
    elif validate_reward_type == "distribute":
        reward_type, action_hash = calculate_ntx_distribution(request)
        return reward_type, action_hash
    else:
        raise ValueError(
            f"Invalid reward_type: {reward_type}. Expected 'withdraw', 'refund', or 'distribute'."
        )


def calculate_ntx_distribution(request):
    number_of_interval = 4
    interval = 100 / number_of_interval
    calculated_rate = (request.job_duration / request.estimated_job_duration) * 100

    if calculated_rate >= interval * 4:  # withdraw case here
        return "withdraw", request.withdraw_hash
    elif calculated_rate < interval:  # refund case here
        return "refund", request.refund_hash
    elif calculated_rate >= interval * 2 and calculated_rate < interval * 3:
        logger.info("Distributing 50-50 NTX between CP and SP")
        return "distribute-50", request.distribute_50_hash
    elif calculated_rate >= interval * 3 and calculated_rate < interval * 4:
        logger.info("Distributing 75-25 NTX between CP and SP")
        return "distribute-75", request.distribute_75_hash
    else:
        raise ValueError("Wrong data provided for NTX distribution")
