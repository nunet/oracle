from http.server import BaseHTTPRequestHandler
from src import oracle, constants
from src.tests.mock_stub import reward_request_list
import logging

logger = logging.getLogger(__name__)

class OracleHealthCheck(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == "/health-check":
            try:
                self.is_oracle_operational()
                content = b"Everything is fine and Oracle is operational"
                status_code = 200
                logger.info("Everything is fine and Oracle is operational")
            except Exception as e:
                logger.error(f"Error while checking health of Oracle: {e}")
                status_code = 500
                content = b"Oracle is not operational as it is getting an error"
        else:
            status_code = 404
            content = b"404 Not Found"
        self.send_response(status_code)
        self.send_header("content-type", "text/html")
        self.end_headers()
        self.wfile.write(content)


    def is_oracle_operational(self):
        oracle.validate_fund_req(constants.SP_ADDR, constants.CP_ADDR, constants.ESTIMATED_PRICE, True)
        oracle.validate_reward_req(reward_request_list["reward_option_1"], True)