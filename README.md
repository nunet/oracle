## Introduction
Now, Oracle is basically working as a validation layer for funding/reward transactions. However,
in the future it may have more functionalities like Price decision and other things.

Note: for now, Oracle is not working alone, we're doing the signature part on a Haskell server (through localhost API communication) until
we achieve to make the signature work with Pycardano. So Oracle is doing everything except with the signing part which is done by Haskell server.

So, there are two things that Oracle does:
- Verify and validate funding/reward request
- Return a signature based on the Oracle wallet

Being specifically, these are the steps that Oracle follows for each request type:
1. Funding request: Receive funding request -> Creates a random message -> Signs message -> Returns signature
2. Reward request: Receive reward request -> Job validation: decide if job was completed or not -> If it was, create random message ->
Signs message -> Returns signature


## Setup
If you clonned the repository without the --recursive tag, please, run the following:

```
git submodule init
git submodule update --remote
```

(This will pull the content from the proto's git submodule)

Dependencies for the following steps: Python and Pipenv

## Run server

First, install the dependencies from the pipfile with:

```
pipenv install
pipenv shell
```

After that, run:

```
 sh compile_proto.sh
```
(This will compile the .proto file and store the generated code on protos/compiled)

Then, please run the following commands to finally run a Oracle gRPC-Server.

```
python main.py
```

Now your Oracle gRPC-Server must be running.

## Test server and client LOCALLY
To run a gRPC server on localhost, you have to run the same command as the production but passing an additional argument as the following:
```
python main.py test
```
(Note: the "test" arg will also sign the Oracle message with a test cardano wallet created for tests)

And to test this localhost gRPC server, we can also run a localhost gRPC client with:
```
python -m src.tests.mock_stub test
```
(Note: don't forget the 'test' argument)

This python script will run a gRPC-Client and make requests to your local gRPC-Server, so your server must be running in parallel.

## Test Oracle running on dev-server (or any other server)
To communicate through your local gRPC client with the Oracle running on the dev server, run the following command (it is the same of above
but without the 'test' arg)
```
python src.tests.mock_stub
```
By the default, if you run without the 'test' arg, this mock client will use the dev server domain and port 40052. But you can change the address
and port for tests purposes on the file: ```src/tests/mock_stub.py```

Note: In our current context, we utilize the following environment variables:

HASKELL_SERVER_URL: This variable represents the URL of the Haskell server that our application interacts with for data retrieval.

## Protobuffer

The .proto file comes from a git submodule called "compute-api-spec", to make alterations to it, go to https://gitlab.com/nunet/open-api/compute-api-spec/-/tree/develop

But the git submodule has just the .proto file, that's why we have to compile with `compile_proto.sh`.

### protoc-gRPC compile bug (for python)
The following is already being solved in the compile bash scripts, I'll explain the bug just to be the user be aware.

(IMPORTANT: you don't need to do the following steps if you are using one of the compile scripts)

* Problem: the generated Python code may not work for some importing issues (see more on https://github.com/protocolbuffers/protobuf/issues/1491). 

* Solution: In the `oracle_pb2_grpc.py` file:

Instead of:
```
import oracle_pb2 as oracle__pb2
```
Change to:
```
from . import oracle_pb2 as oracle__pb2
```
