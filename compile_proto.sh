dir=protos/compiled

if [ ! -d "$dir" ]; then
    mkdir protos/compiled
fi

python -m grpc_tools.protoc \
-Iprotos/compute-api-spec \
--python_out=protos/compiled \
--pyi_out=protos/compiled \
--grpc_python_out=protos/compiled protos/compute-api-spec/oracle.proto
 
# Fix protoc-gRPC compile bug (see more on README.md)
sed 's/^import oracle_pb2 as oracle__pb2/from \. import oracle_pb2 as oracle__pb2/' protos/compiled/oracle_pb2_grpc.py > /tmp/workaround.py && cat /tmp/workaround.py > protos/compiled/oracle_pb2_grpc.py
