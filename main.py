import logging
import sys
from concurrent import futures
import grpc
from protos.compiled import oracle_pb2, oracle_pb2_grpc
from src import oracle
from http.server import HTTPServer
import threading
from src.http_api import OracleHealthCheck

if len(sys.argv) == 2 and sys.argv[1] == "test":
    address = "localhost"
    grpc_port = "50052"
    metrics_port = "9080"
    is_test_env = True
else:
    address = "0.0.0.0"
    grpc_port = "50052"
    metrics_port = "9080"
    is_test_env = False


class OracleServicer(oracle_pb2_grpc.OracleServicer):
    def ValidateFundingReq(self, request, context) -> oracle_pb2.FundingResponse:
        if is_test_env:
            logging.warning("Running on TEST mode. Processing funding request")

        logging.info("Funding request validation received. ")
        params_info = f"\nService Provider: {request.service_provider_addr}"
        params_info += f"\nCompute Provider: {request.compute_provider_addr}"
        params_info += f"\nEstimated Price: {request.estimated_price}"
        logging.info(f"Received params:{params_info}")
        try:
            (
                meta_hash,
                withdraw_hash,
                refund_hash,
                distribute_50_hash,
                distribute_75_hash,
            ) = oracle.validate_fund_req(
                request.service_provider_addr,
                request.compute_provider_addr,
                request.estimated_price,
                is_test_env=is_test_env,
            )
            logging.info("Funding process completed, returning the hashes to DMS")
            return oracle_pb2.FundingResponse(
                metadata_hash=meta_hash,
                withdraw_hash=withdraw_hash,
                refund_hash=refund_hash,
                distribute_50_hash=distribute_50_hash,
                distribute_75_hash=distribute_75_hash,
            )
        except Exception as e:
            error_msg = f"Error on funding request: {e}"
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(error_msg)
            logging.warning(error_msg)
            return oracle_pb2.FundingResponse()

    def ValidateRewardReq(self, request, context) -> oracle_pb2.RewardResponse:
        if is_test_env:
            logging.warning("Running on TEST mode. Processing reward request")

        logging.info("Reward request validation received. ")
        params_info = f"\nJob Status: {request.job_status}"
        params_info += f"\nLog Path: {request.log_path}"
        logging.info(f"Received params:{params_info}")
        try:
            (
                reward_type,
                sign_datum,
                msg_hash_datum,
                datum,
                sign_action,
                msg_hash_action,
                action,
            ) = oracle.validate_reward_req(request, is_test_env=is_test_env)
            logging.info("Reward process completed, returning the results to DMS")
            return oracle_pb2.RewardResponse(
                reward_type=reward_type,
                signature_datum=sign_datum,
                message_hash_datum=msg_hash_datum,
                datum=datum,
                signature_action=sign_action,
                message_hash_action=msg_hash_action,
                action=action,
            )
        except Exception as e:
            error_msg = f"Error while processing reward request: {e}"
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(error_msg)
            logging.warning(error_msg)
            return oracle_pb2.RewardResponse()


def run_grpc_server(address, port):
    # TODO: use gRPC's ALTS (for security)
    # TODO: how many max_workers is sufficient
    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=40), maximum_concurrent_rpcs=80
    )
    oracle_pb2_grpc.add_OracleServicer_to_server(OracleServicer(), server)
    server.add_insecure_port(f"{address}:{port}")
    server.start()
    print(f"INFO: GRPC Server is up and running on port {port}")
    server.wait_for_termination()


def run_rest_server(address, port):
    server_addr = (address, int(port))
    httpd = HTTPServer(server_addr, OracleHealthCheck)
    print(f"INFO: REST Server is up and running on port {port}")
    httpd.serve_forever()


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s: %(message)s"
    )
    threading._start_new_thread(run_rest_server, (address, metrics_port))
    run_grpc_server(address, grpc_port)
