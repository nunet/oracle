# syntax = docker/dockerfile:1
# For more information, please refer to https://aka.ms/vscode-docker-python
ARG PYTHON_VERSION=3.10

FROM python:${PYTHON_VERSION}-slim

ARG CI_MERGE_REQUEST_TARGET_BRANCH_NAME

ARG CI_COMMIT_BRANCH

RUN apt update && apt install git -y

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install packages using requirements.txt
COPY requirements.txt ./
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

WORKDIR /app

COPY . /app

RUN <<EOT
    if [ "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" = 'staging' ] || [ "$CI_COMMIT_BRANCH" = 'staging' ];then 
        git clone -b staging https://gitlab.com/nunet/open-api/compute-api-spec.git/ protos/compute-api-spec
        # git submodule set-branch --branch staging protos/compute-api-spec; 
    elif [ "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" = 'main' ] || [ "$CI_COMMIT_BRANCH" = 'main' ];then
        git clone -b main https://gitlab.com/nunet/open-api/compute-api-spec.git/ protos/compute-api-spec 
        # git submodule set-branch --branch main protos/compute-api-spec; 
    else 
        git clone https://gitlab.com/nunet/open-api/compute-api-spec.git/ protos/compute-api-spec
        # git submodule set-branch --branch develop protos/compute-api-spec; 
    fi
EOT

# RUN cat .gitmodules

# RUN git submodule foreach git config --get remote.origin.fetch

# RUN git submodule foreach --recursive git pull

# RUN git submodule update --init --remote

# Creates a non-root user and adds permission to access the /app folder
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

EXPOSE 9080 50052

RUN sh compile_proto.sh

CMD ["python", "main.py"]
